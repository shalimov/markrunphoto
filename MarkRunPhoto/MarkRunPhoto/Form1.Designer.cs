﻿namespace MarkRunPhoto
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.textBoxControlPhotoFile = new System.Windows.Forms.TextBox();
            this.buttonExplore = new System.Windows.Forms.Button();
            this.numericHour = new System.Windows.Forms.NumericUpDown();
            this.numericMin = new System.Windows.Forms.NumericUpDown();
            this.numericSec = new System.Windows.Forms.NumericUpDown();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.buttonGo = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.numericHour)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericMin)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericSec)).BeginInit();
            this.SuspendLayout();
            // 
            // textBoxControlPhotoFile
            // 
            this.textBoxControlPhotoFile.Location = new System.Drawing.Point(12, 12);
            this.textBoxControlPhotoFile.Name = "textBoxControlPhotoFile";
            this.textBoxControlPhotoFile.ReadOnly = true;
            this.textBoxControlPhotoFile.Size = new System.Drawing.Size(370, 20);
            this.textBoxControlPhotoFile.TabIndex = 0;
            // 
            // buttonExplore
            // 
            this.buttonExplore.Location = new System.Drawing.Point(388, 10);
            this.buttonExplore.Name = "buttonExplore";
            this.buttonExplore.Size = new System.Drawing.Size(138, 23);
            this.buttonExplore.TabIndex = 1;
            this.buttonExplore.Text = "Обзор...";
            this.buttonExplore.UseVisualStyleBackColor = true;
            this.buttonExplore.Click += new System.EventHandler(this.buttonExplore_Click);
            // 
            // numericHour
            // 
            this.numericHour.Location = new System.Drawing.Point(13, 39);
            this.numericHour.Maximum = new decimal(new int[] {
            23,
            0,
            0,
            0});
            this.numericHour.Name = "numericHour";
            this.numericHour.Size = new System.Drawing.Size(36, 20);
            this.numericHour.TabIndex = 2;
            // 
            // numericMin
            // 
            this.numericMin.Location = new System.Drawing.Point(77, 38);
            this.numericMin.Maximum = new decimal(new int[] {
            59,
            0,
            0,
            0});
            this.numericMin.Name = "numericMin";
            this.numericMin.Size = new System.Drawing.Size(36, 20);
            this.numericMin.TabIndex = 2;
            // 
            // numericSec
            // 
            this.numericSec.Location = new System.Drawing.Point(155, 38);
            this.numericSec.Maximum = new decimal(new int[] {
            59,
            0,
            0,
            0});
            this.numericSec.Name = "numericSec";
            this.numericSec.Size = new System.Drawing.Size(36, 20);
            this.numericSec.TabIndex = 2;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(56, 45);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(15, 13);
            this.label1.TabIndex = 3;
            this.label1.Text = "ч.";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(197, 45);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(28, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "сек.";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(119, 45);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(30, 13);
            this.label3.TabIndex = 3;
            this.label3.Text = "мин.";
            // 
            // buttonGo
            // 
            this.buttonGo.Location = new System.Drawing.Point(13, 125);
            this.buttonGo.Name = "buttonGo";
            this.buttonGo.Size = new System.Drawing.Size(513, 23);
            this.buttonGo.TabIndex = 4;
            this.buttonGo.Text = "Поехали!";
            this.buttonGo.UseVisualStyleBackColor = true;
            this.buttonGo.Click += new System.EventHandler(this.buttonGo_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(790, 262);
            this.Controls.Add(this.buttonGo);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.numericSec);
            this.Controls.Add(this.numericMin);
            this.Controls.Add(this.numericHour);
            this.Controls.Add(this.buttonExplore);
            this.Controls.Add(this.textBoxControlPhotoFile);
            this.Name = "Form1";
            this.Text = "Form1";
            ((System.ComponentModel.ISupportInitialize)(this.numericHour)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericMin)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericSec)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox textBoxControlPhotoFile;
        private System.Windows.Forms.Button buttonExplore;
        private System.Windows.Forms.NumericUpDown numericHour;
        private System.Windows.Forms.NumericUpDown numericMin;
        private System.Windows.Forms.NumericUpDown numericSec;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button buttonGo;
    }
}

