﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Text;
using System.Windows.Forms;
using System.Windows.Media.Imaging;

namespace MarkRunPhoto
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void buttonExplore_Click(object sender, EventArgs e)
        {
            OpenFileDialog ofd = new OpenFileDialog();
            if (ofd.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                textBoxControlPhotoFile.Text = ofd.FileName;
            }
        }

        private void buttonGo_Click(object sender, EventArgs e)
        {
            try
            {
                buttonGo.Enabled = false;


                String controlFileName = textBoxControlPhotoFile.Text;

                if (controlFileName == null || controlFileName.Length == 0)
                {
                    throw new SomeException("Нужно указать контрольный файл");
                }

                DateTime controlPhotoTaken = getExifPhotoDateTaken(controlFileName);
                TimeSpan controlPhotoOffset = new TimeSpan(
                    (int)numericHour.Value,
                    (int)numericMin.Value,
                    (int)numericSec.Value
                    );

                DateTime startTime = controlPhotoTaken.Subtract(controlPhotoOffset);

                String directoryName = Path.GetDirectoryName(controlFileName);
                string[] files = Directory.GetFiles(directoryName, "*.jpg");

                int i = 0;
                int total = files.Length;

                foreach (string file in files)
                {
                    TimeSpan diff = GetPhotoTimeFromStart(startTime, file);

                    String newFileName = Path.Combine(
                        directoryName,
                        "[" + formatTimeSpanForFilename(diff) + "] " + Path.GetFileName(file)
                        );

                    File.Move(file, newFileName);

                    i++;
                    buttonGo.Text = String.Format("{0}/{1} файлов обработано", i, total);
                }
            }
            catch (SomeException exc)
            {
                MessageBox.Show(exc.Message);
            }
            catch (Exception exc)
            {
                MessageBox.Show(exc.Message + "\n" + exc.StackTrace);
            }
            finally
            {
                buttonGo.Text = "Поехали";
                buttonGo.Enabled = true;
            }

        }



        private string formatTimeSpanForFilename(TimeSpan timeSpan)
        {
            return string.Format("{0:00}-{1:00}-{2:00}",
               (int)timeSpan.TotalHours,
                    timeSpan.Minutes,
                    timeSpan.Seconds);
        }


        private TimeSpan GetPhotoTimeFromStart(DateTime startTime, string photoFile)
        {
            DateTime exifDatePhotoTaken = getExifPhotoDateTaken(photoFile);
            if (exifDatePhotoTaken < startTime) return new TimeSpan(0, 0, 0);
            TimeSpan diff = exifDatePhotoTaken - startTime;
            return diff;
        }


        public DateTime getExifPhotoDateTaken(String filename)
        {            
            FileStream fs = new FileStream(filename, FileMode.Open, FileAccess.Read, FileShare.Read);
            BitmapSource img = BitmapFrame.Create(fs);
            BitmapMetadata md = (BitmapMetadata)img.Metadata;
            string dateStr = md.DateTaken;
            fs.Close();
            Console.WriteLine(dateStr);

            try
            {
                return DateTime.ParseExact(dateStr, "dd.MM.yyyy HH:mm:ss", CultureInfo.InvariantCulture);
            }
            catch (FormatException e)
            {
                throw new SomeException("Не удалось распознать дату в EXIF для файла " + Path.GetFileName(filename) + " (" + dateStr + ")\n" + e.Message);
            }
        }
    }



    class SomeException : Exception
    {
        public SomeException(String msg) : base(msg)
        {

        }
    }
}
